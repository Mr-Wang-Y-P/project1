# 手写jsonp

- 跨域
    后端处理
    1. 设置响应头 Access-Control-Allow-Origin
        res.header('Access-Control-Allow-Origin','*')
        cors ajax 同源策略
    2. 不受同源机制限制的 script 标签
        <script src="外网地址"></script>
        img
        link
        链接外界静态资源的

- CORS Policy
    cross origin resource share
    安装 npm i cors
  - 为了安全性
        企业内部，baidu.com api.baidu.com zhidao.baidu.com
        前后端分离 3000 8080 跨域是家常便饭
        cors 方案 适用于 比较保守的跨域解决问方案
        公司内部 获友商
        cors 抽象的中间件 cors

- script 门户开放型
    开放给全网用的 github api douban api
    origin 起始是在哪？ 用户的浏览器端  同源策略
    1. script 可以请求跨域资源 不受同源策略的影响
        前提是返回js
    2. 提前埋好 callback 回调函数
        返回的js 会执行

    json 资源 外面包含一个函数 成为js 函数调用