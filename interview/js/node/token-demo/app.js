const Koa = require('koa'); // node 里的框架 commonjs 模块
const router = require('koa-router')();
const app = new Koa(); //  服务器端应用
const path = require('path') // 内置模块
const views = require('koa-views')

// 挂载模板引擎
app.use(views(
    // 模板位置和 引擎
    path.join(__dirname, './views'), {
        extension: 'ejs' // 模板引擎的种类
    }
))
// 后端的登录界面 传统的web 网站开发 
router.get('/login', async (ctx) => {
    // ctx 本次服务的上下文环境对象 = request + response 
    // 伺服状态， 怎么响应用户请求 
    // response
    const title = 'hello koa2'
    // 页面的渲染  MVC  View Controller 
    await ctx.render('index', {
        title
    })
    // ctx.body = {
    //     msg:'hello, login'
    // }
})

app
    .use(router.routes()) // vue 路由数组
    .use(router.allowedMethods()) // get post delete put patch ... 

app.listen(8080, () => {
    console.log('server is running 8080')
})

