const WebSocket = require('ws') // 安装 npm i ws 基于tcp/ip
// const http = require('http') // node 内置的
// 在某个端口启动的程序
// ip 定位服务器 端口 这台服务器启动的某个进程 -> 某个程序
const wss = new WebSocket.Server({
    port:3000
})
wss.on('connection',(ws)=>{
    ws.on('message',(message)=>{
        // console.log('message',message);
        let msgData = JSON.parse(message)
        wss.clients.forEach(client => {
            client.send(message)
        })
    })
})