# websocket

- HTTP 优点和缺点
    HTTPS
    简单 请求和响应 无状态
    http 1.0 cookie

    单向的，服务器端不能主动推消息

- http 不可以，但 websocket 可以
    html5 新功能
  - 语义化标签
  - form input 类型
        type="number|range| placeholder required"
  - video audio canvas
  - localStorage SessionStorage IndexDB
  - drap drop
  - lbs 获得用户地理位置信息 geolocation
  - web workers js 多线程
  - websocket 实时通信
  - Blob 二进制流对象
  - File 对象 FileReader
  
- 使用http 协议 启动web 服务 得到首页   与服务器断开链接
  - a 链接 重新请求 服务器处于被动的伺服状态
  - ajax 本质 http
- WebSocket 启动了 WebSocket 请求
  - 建立链接
  - 双向通信
