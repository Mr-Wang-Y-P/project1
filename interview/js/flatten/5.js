// 手写call es6 版本会比老版本更优雅
Function.prototype.call = function (context,...args) {
    context = context || window
    let fn = Symbol('fn');  // 唯一值
    // 给context 动态添加fn 属性
    context.fn = this;  //绝对不会覆盖 context 上的属性

    // let args = []; //函数原来的参数
    // for (let i = 1, len = arguments.length; i < len; i++) {
    //     args.push('arguments['+i+']')
    // }
    // let result = context.fn(...args);
    //eval 保证函数立即执行
    let result = eval('context.fn(...args)')
    delete context // 让context 干净 代码洁癖
    return result
}

const mouse = {name:'Jerry'}
function say(a,b,c){
    console.log(`Tom like ${this.name ||'default'}`,a,b,c)
}

console.log(say.call(mouse,1,2,3))