// 赋值
// let a=1
// let b=a;
// b=3;
// console.log(a,b); //1 3

// let arr =[1,2,3]
// let newArr = arr; //赋值 不是拷贝 同一个内存地址引用
// newArr[0]=100;
// console.log(arr,newArr); //[ 100, 2, 3 ] [ 100, 2, 3 ]


let arr = [1,2,{val:4}]
//浅拷贝
// let newArr =arr.slice();
// newArr[0]=100;
// console.log(arr,newArr); //[ 1, 2, { val: 4 } ] [ 100, 2, { val: 4 } ]
// // 复杂数据类型 
// newArr[2].val=5;
// console.log(arr,newArr); //[ 1, 2, { val: 5 } ] [ 1, 2, { val: 5 } ]

// 深拷贝
// 循环引用 内存溢出
// 函数等不支持序列化 直接忽略
const newArr = JSON.parse(JSON.stringify(arr));
newArr[2].val=5;
console.log(arr,newArr); //[ 1, 2, { val: 4 } ] [ 1, 2, { val: 5 } ]

