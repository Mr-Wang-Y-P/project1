let arr = [1, [2, [3, [4, 5]]], 6];

let str = JSON.stringify(arr); //序列化

// let o ={a:1,b:2,c:3}

//ES10 内置 flat API
//ES10 中的 flat ⽅法来实现数组扁平化。
//flat ⽅法的语法：arr.flat ( [depth] )
//其中 depth 是 flat 的参数，depth 是可以传递数组的展开深度（默认不填、数值是 1），
//即展开⼀层数组。如果层数不确定，参数可以传进 Infinity，代表不论多少层都要展开：

// arr = arr.flat(Infinity); //es10(2019) 提供的flat API 存在兼容性问题
// console.log(arr);

//正则表达式
// (\[|\])
//  / / 是 正则的格式 
//  g 正则修饰符 全局  不停下来 一直匹配
// () 分组 匹配的各种可能 放到这个分组里
// | 或者 正则是按字符匹配的  [a-z]   匹配一个小写字母  [a-z] {3,5} 匹配3-5 五个字符
// [ 本身是正则的运算符号  \[ 转义一下  匹配字符串 本身
// replace 方法 正则匹配到的替换成 空
// console.log(str); 
// // arr= str.replace(/(\[|\])/g,'').split(',');
// // console.log(arr.map(Number));
// arr= str.replace(/(\[|\])/g,'').split(',').map(item => +item);
// console.log(arr);

// str= str.replace(/(\[|\])/g,'')
// str= '['+str+']' //字符串拼接
// arr = JSON.parse(str) //反序列化
// console.log(str,arr)

// 递归
// let result=[]
// let fn= function(arr){
//     for(let i=0;i<arr.length;i++){
//         let item =arr[i]
//         if(Array.isArray(arr[i])){
//             fn(item)
//         }else {
//             result.push(item)
//         }
//     }
// }
// fn(arr)
// console.log(result)

// cur 指arr中的 各个元素  arr[i] 
// [] 初始值
// pre 前一次调用 callbackfn 得到的返回值
// function flatten(arr){
//     return arr.reduce((pre,cur)=>{
//         return pre.concat(Array.isArray(cur)? flatten(cur):cur)
//     },[])
// }
// console.log(flatten(arr))

// let arr = [1, [2, [3, [4, 5]]], 6];
while(arr.some(Array.isArray)){
    arr=[].concat(...arr)
}
console.log(arr);