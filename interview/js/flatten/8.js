let obj= {name: 'sy',age:18,hometown:{
    name:'乐平'
}}

const obj2 = Object.assign({},obj,{name:'sss'})  // {} 是要返回的目标对象，接收源对象属性的对象，也是修改后的返回值。
//浅拷贝
console.log(obj,obj2);  // { name: 'sy', age: 18 } { name: 'sss', age: 18 }
obj2.age=20;
console.log(obj,obj2); // { name: 'sy', age: 18 } { name: 'sss', age: 20 }

obj2.hometown.name='景德镇'
console.log(obj,obj2); //{ name: 'sy', age: 18, hometown: { name: '景德镇' } } { name: 'sss', age: 20, hometown: { name: '景德镇' } }
//对象中的简单值 赋值 不会影响原对象 对象中的对象 拷贝 还是拷贝的是地址 会影响原对象