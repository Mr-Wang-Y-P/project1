- 组成任务流 Promise序列
    [upload(),upload(),upload(),upload()]
  - 随机定时器值 任务完成时间 和索引没有关系
  - 一半成功 一半失败
  - setTimeout 第三个参数会在定时器到达时触发
  - promise then 最后catch rejected 时候触发
  - async await
        rejected
        try{

        }
        catch{

        }
  - 是否运行了 
     console.log(`run ${time}ms`);
  - Promise.all
    全部执行通过 才认为是成功 否则认为是失败
    有失败的并不会停下来
    结果 是按序列 与时间没有关系
    - 并发
  - Promise.race 
    - 第一个完成的 result
    - 所有的都会运行
    - 并行
  - Promise.any
    -  只要有一个成功就行 返回最先成功的