const express = require('express')
const app = express() // 后端应用App
const port = 3000
// 添加路由
// http 是一个简单协议 基于tcp/ip 基于请求响应
// 使用上网代理(浏览器)，输入地址 http://localhost:3000/say

// 请求行 get 地址 http://localhost:3000/say http 版本号
// 请求头 cookie Authorization

// 后端放水
// let whiteList = ['http://127.0.0.1:5500']

// app.use((req,res,next) =>{
//     // 哪些跨域请求通过 origin 不太了解 * 所有都通过 or
//     let origin=req.headers.origin
//     console.log(origin,'|||')
//     if(whiteList.includes(origin)){
//         res.setHeader('Access-Control-Allow-Origin',origin)
//         // DEL 删除 PUT 整个替换 PATCH 部分修改   不能修改
//         // Restful 一切皆资源 请求资源的时候 使用相应的方法
//         res.setHeader('Access-Control-Allow-Methods','Get,Post')
//         res.setHeader('Access-Control-Allow-Credentials','true')
//     }
//     // res.setHeader('Access-Control-Allow-Origin','*')
//     console.log('-----')
//     next()
// }) //中间件
// app.get('/say', (req, res) => res.send('Hello World!'))
app.get('/say',function(req,res){
    let jsonData={
        name:'蔡总帅',
        favor:['喝酒','泡吧']
    }
    // let a='hello'
    // res.setHeader('Content-Type','text/json;charset=utf-8')
    // res.send(JSON.stringify(jsonData))
    res.send('callBack('+ JSON.stringify(jsonData) +')')
    // res.send('callBack('+ JSON.stringify(a) +')')
})
app.listen(port)