# 前端性能优化最重要-- 缓存

- 监听资源缓存
    304     Not Modified

- 第一次访问某站
   200 返回资源 花费时间

- node 的理解
   1. 如何返回 html 模板
    硬盘里 fs 读出来 字符串
    fs.readFile 异步的 不会阻塞node 单线程  进入到 node 的eventloop
    同样硬件的能力下 多让一些用户访问 服务器价格便宜
    index.js 内存 -> fs(I/O) -> 硬盘 -> 回调函数 -> 第一个参数是err(文件路径存在，固态硬盘和机械硬盘的区别)，第二个是data数据

   2. readFileSync 同步，不太用，性能不好
   3. 流的概念
        二进制文件的处理思想
        createReadStream

- 图片格式
  jpg png(透明)
  webp 同样的清晰度，体积只有 1/2 1/3

- 缓存
   1. 服务器端开启缓存 强缓存
      - 返回资源的同时 Expires 过期时间 setHeader('Expires'，时间)
        本地缓存这个图片时，Exipres 写入
      - 再次请求 /two.png 前端缓存就挡道
        本地的时钟和缓存的 Exipres 时间比较 强制走缓存 或 走后端
      - 本地的时钟不准确  Expires 受限于本地时间，如果修改了本地时间，就会造成缓存失效。

   2. HTTP 1.1 更新 Cache-Control 相对时间
        max-age 倒计时

- 强缓存
   expires http 1.0
   cache-control max-age

- 缓存过期了 怎么办？一定要重新缓存吗？
   强缓存过期 但是服务器文件 变了 or 没变

- 协商缓存
   Last-Modified + If-Modified-Since
   Etag + If-None-Match
