const http = require('http')
const fs = require('fs')
const path = require('path')
const server = http.createServer((req, res) => {
    // console.log(req.url);
    if (req.url == '/') {
        // console.log(req.url);
        // fs.readFile('./index.html','utf-8',(err,data)=>{
        //     //node 异步 无阻塞的
        //     // 默认 第一个参数 err
        //     if(err){
        //         res.end('出错了')
        //         return
        //     }
        //     // console.log(data);
        //     res.end(data)
        // })
        // Async 异步 sync 同步
    //     try{
    //         const data = fs.readFileSync('./index.html','utf-8')
    //         console.log(data);
    //     }
    //    catch{
    //     console.error(err)
    //    }
    fs.createReadStream(path.join(__dirname,'index.html')).pipe(res)
    return 
    }
//    console.log(req.url);
let abs = path.join(__dirname,req.url)
//文件或目录信息
fs.stat(abs,(err,stat)=>{
    if(err){
        res.statusCode=404
        res.end('Not Found')
        return
    }
    // linux 文件和文件夹 都是fs 一样的 区别是头信息 是否是文件
    if(stat.isFile()){
        // res.setHeader('Expires',new Date(Date.now()+10000).toGMTString())
      
        res.setHeader('CacheControl','max-age=30')
        fs.createReadStream(abs).pipe(res)
    }
})
console.log(abs);
})


server.listen(3000)