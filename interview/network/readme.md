# 三次握手 四次挥手

- B/S 架构
  C/S 架构
  - 只需要一个url 不需要安装
  - C/S 要适配系统 微软 linux mac 不一样
    java html/css/js

- TCP/IP
  OSI 七层协议 五层
  IP 唯一性
  TCP/UDP   确保数据传输正确且完整

- HTTP 应用层协议 基于TCP/IP 的
    三次握手 用于建立连接
        确认 双方都有发送及接受的能力
    四次挥手 断开连接
    HTTP 是无状态的 会关闭
