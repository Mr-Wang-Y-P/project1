const root = {
    val: "A",
    left: {
        val: "B",
        left: {
            val: "D"
        },
        right: {
            val: "E"
        }
    },
    right: {
        val: "C",
        right: {
            val: "F"
        }
    }
};
// 后进先出 
// A 
// {c :f},e,d
// {d,e}
// A b,d,e,c,f

const preorderTraversal = function(root){
    const res =[]
    if(!root){
        return res
    }
    const stack =[] // 栈辅助
    stack.push(root) // 根先入栈
    while(stack.length){ //栈不为空
        const cur = stack.pop() // cur -> root
        res.push(cur.val)
        if(cur.right){
            stack.push(cur.right)
        }
        if(cur.left){
            stack.push(cur.left)
        }
    }

    return res
}

// while(stack.length) {
//     // 将栈顶结点记为当前结点
//     const cur = stack.pop() 
//     // 当前结点就是当前子树的根结点，把这个结点放在结果数组的头部
//     res.unshift(cur.val)
//     // 若当前子树根结点有右孩子，则将右孩子入栈
//     if(cur.right) {
//       stack.push(cur.right)
//     }
//     // 若当前子树根结点有左孩子，则将左孩子入栈
//     if(cur.left) {
//       stack.push(cur.left)
//     }
//   }