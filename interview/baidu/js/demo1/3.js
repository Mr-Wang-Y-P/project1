const a = function (s, inputStr) {

    const b = function (str) {
        let arr = []
        str = str.split('').sort((a, b) => {
            return a > b ? 0 : 1
        }).join('')
        const dfs = function (curr, store) {
            if (!store.length) {
                return arr.push(curr)
            }
           for(let i=0;i<store.length;i++){
            if(i>0 && store[i]===store[i-1]) continue
            dfs(curr+store[i],store.slice(0,i)+store.slice(i+1))
           }
        }
        dfs('', str)
        return arr
    }
    const res =b(s)
    return res.includes(inputStr)
}

console.log(a('alibaba','babalia'));