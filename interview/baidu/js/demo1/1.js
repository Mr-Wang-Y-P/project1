const isPermutatuinExisted=(s,inputStr)=>{
const permutatuin = (str) =>{
    let ans = [] //任何排列结果放这个数组里面
    str=str.split('').sort((a,b) =>{
        return a>b ? 1 : -1  //从小到大排列
    }).join('') // 切割成数组
    // console.log(str);
    // 深度优先搜索
    // 递归
    // dfs('',str) //当时的状态 开始是空字符串，后面是整个字符串
    // curr 当前已选择的字符串 store 待选则的字符
    const dfs = (curr,store) =>{
        // 退出条件
        if(!store.length){
            return ans.push(curr)
        }
        for(let i=0;i<store.length;i++){
            if(i > 0 && store[i]===store[i-1]) continue
            dfs(curr+store[i],store.slice(0,i)+store.slice(i+1)) // a b|cv3  ab c ac b abc 
        }
    }
    dfs('',str)
    return ans;
    // return []
}
const res=permutatuin(s)
return res.includes(inputStr)
}
// console.log(permutatuin('baidu'))
// permutatuin('baidu').indexOf('iabdu')
console.log(isPermutatuinExisted('baidu','iabdu'))