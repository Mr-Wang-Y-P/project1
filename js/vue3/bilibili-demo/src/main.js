import { createApp } from 'vue'
import router from './router'
import App from './App.vue'
import '@/mock/index'
import { createPinia } from 'pinia'
import {
  Lazyload,
  Swipe,
  SwipeItem
} from 'vant'
import 'vant/lib/index.css'
import 'lib-flexible/flexible' //rem
import './assets/main.css'
const app = createApp(App)

app
  .use(router)
  .use(createPinia())
  .use(SwipeItem)
  .use(Swipe)
  .use(Lazyload)
  .mount('#app')
