import { createRouter, createWebHashHistory} from 'vue-router'
import Home from '../views/Home.vue'
// import List from '../views/List.vue'
const routes = [
    {
        page: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/list',
        name: 'list',
        meta:{
            keepAlive:true
        },
        component: ()=>import('../views/List.vue') //按需加载
        }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes,
   
})

export default router