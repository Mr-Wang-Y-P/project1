import { createRouter, createWebHashHistory} from 'vue-router'
import Home from '../views/Home.vue'
// import List from '../views/List.vue'
const routes = [
    {
        page: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/list',
        name: 'list',
        component: ()=>import('../views/List.vue') //按需加载
        }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes,
    // 自动设置每个页面的滚动条
    scrollBehavior(to,from){
        if(to.name==='list'){
            // 分页面对待
            return{
                top:parseInt(localStorage.getItem('scroll'))
            }
        }
    }
})

export default router