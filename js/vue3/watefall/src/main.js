import { createApp } from 'vue'

import App from './App.vue'
import 'lib-flexible/flexible'
import './mock/index.js'
import mockjs from 'mockjs'
createApp(App)
            .use(mockjs)
            .mount('#app')
 