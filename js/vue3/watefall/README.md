# 瀑布流组件开发

- 电商类首页移动端 采用瀑布流效果
    错落有致 美观
- 大厂考题 特定组件封装
    高难度
- 编程里碰到难题 怎么解决？
    - 细分法 分治思想 分步
        1. 两列式弹性布局
            flex + jc space-between
        2. vue 数据绑定
            allGoods
            {
                pic:'',
                title:'',
                height:''
            }
            leftGoods
            rightGoods
        3. 哪一列最短 把数据放到哪一列