import { effect, track } from "./effect"

const get = createGetter()
const set = () => { }
const has = () => { }
const deleteProperty = () => { }

// shallow 浅
function createGetter(shallow = false) {
    return function get(target, key, receiver) {
        // 本职 返回普通对象的值
        // targetMap -> object -> key -> [effect() ,effect2(),]
        // es6 提供的映射api
        // target[key]
        const isExistMap = () => key === ReactiveFlags.RAW

        const res = Reflect.get(target, key, receiver)
        // console.log(target, key, receiver, res, '????');
        // console.log(target[key], '---');
        // console.log(target.key, '---undefined');
        // console.log(target == receiver,'false'); 
        // console.log(target.key == res,'false');
        track
        return res
    }
}
export const mutableHandlers = {
    get,
    set,
    has,
    deleteProperty
}