import axios from "axios"
export const getCategoryData = (params)=>{
    return axios.get('/categories', params)
}