export const getPageTitle= (pathName)=>{
    const data={
        login:'登录',
        introduce:'系统介绍',
        add:'添加商品'
    }
    return data[pathName] || ''
}
export const getLocal =(name)=>{
    // console.log(name)
    return JSON.parse(localStorage.getItem(name))
}
//html5 提供的本地KeyValue 存储
export const setLocal =(key,val)=>{
    // console.log(name)
    localStorage.setItem(key,JSON.stringify(val))
}