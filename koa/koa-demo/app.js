const Koa = require('koa')
const config = require('./config/default.js')
const app = new Koa()
var path = require('path')
var staticCache = require('koa-static-cache')
const views = require('koa-views')
const signupRouter = require('./routers/signup.js')
const signinRouter = require('./routers/signin.js')
const postsRouter = require('./routers/posts.js')
const bodyParser = require('koa-bodyparser')
app.use(staticCache(path.join(__dirname,'./public'),{dynamic:true},{maxAge:15*24*60*60}))
// views 在哪里
app.use(views(path.join(__dirname,'./views'),{
    extension:'ejs'
}))
app.use(bodyParser({
    formLimit: '1mb'
}))
// 洋葱模型
// 中间件为其服务 餐厅
// blog 网站 复杂事情
// 一个函数解决一个问题
// app.use((ctx,next)=>{ // 服务员
//     console.log(1); 
//     next()
//     console.log(6);
//     // 如何向下传
//     // ctx.response.body='hello world'
// })

// app.use((ctx,next)=>{ // 厨师 
//     console.log(2);
//     next()
//     console.log(5);
// })

// app.use((ctx,next)=>{ // 前台 
//     console.log(3); // 前面不加next 就执行不过来
//     // 最后一个服务 执行
//     // ctx.response.body = 'hello world'
//     next()
//     console.log('---',4);  // 感觉有点像先往内 再往外
// })

//  如何记录一个请求所花时间

// 第一个 计时开始
// app.use(async (ctx,next)=>{
//     console.log('中间件1');
//     const start = new Date().getTime();
//     await next()
//     const end = new Date().getTime();
//     console.log('请求花费时间',end - start,'ms');
// })

// app.use(async (ctx,next)=>{
//     console.log('中间件2');
//     const data = await getData();
//     ctx.body ={data}
// })

// const getData = async ()=>{
//     await new Promise((resolve) =>{
//         setTimeout(() => {
//             resolve()
//         }, 1000);
//     })
//     return 'Hello World'
// }

app.use(signupRouter.routes())
app.use(signinRouter.routes())
app.use(postsRouter.routes())
app.listen(config.port)

console.log(`listening on port ${config.port}`);