// indexed.js -> indexed.ts
// typescript = js + 强类型 type 类型

import {createRouter,createWebHistory,RouterOptions} from 'vue-router'
import HomeView from '../views/HomeView.vue'


const router = createRouter({
    history:createWebHistory(),
    routes:[
        {
            path:'/',
            name:'home',
            component:HomeView
        }
    ]
})

//路由管理对象
export default router