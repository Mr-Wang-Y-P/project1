import {ref} from 'vue'
import { defineStore } from 'pinia';
import { gerBanners } from '@/service/recommend';
export const useRecommendStore = defineStore('recommend',()=> {
    const banners =ref([])
    const getBannersData=async()=>{
        const data =await gerBanners()
        console.log(data,'///')
        banners.value=data.banners
    }
    return {
        banners,
        getBannersData
    }
});
