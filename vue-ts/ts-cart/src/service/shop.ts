//js 如何提升数据的完整性
// let num:Number=1112
// num = 'ddd'
// 接口 接头 约束
// JS 为什么没有？interface
// js 弱类型 有太多的糟粕 接口肯定也没有
// 自定义的 检查数据 代码在运行前就报错
//SQL 定义表

// VScode 用ts写的
// js有时候错误都不知道怎么发生的 ts 99.99 不出错
export interface IProduct{
    id:number;
    title:string;
    price:number;
    inventory:number;
}
const _products :IProduct[]=[
    {
        id:1,
        title:'IPad Mini ',
        price:500.01,
        inventory:2
    },
    {
        id:2,
        title:'H&M T-shirt White',
        price:100.51,
        inventory:10
    },
    {
        id:3,
        title:'Charli XCX -Sucker CD',
        price:19.99,
        inventory:0
    },
];

function wait(delay:number){
    return new Promise((resolve)=>{
        setTimeout(resolve, delay);
    })
}

export const getProducts = async()=>{
    //延迟加载
    await wait(1000)
    return _products
}

export const buyProducts = async()=>{
    //延迟加载
    await wait(1000)
    return Math.random()>0.5
}
